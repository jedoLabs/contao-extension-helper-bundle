<?php

/*
 * This file is part of ContaoExtensionHelperBundle.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package     ContaoExtensionHelperBundle
 * @author      Jens Doberenz <WurzelGnOOm>
 * @copyright   jedoLabs 2019 <https://jedo-Labs.de>
 * @license     LGPL-3.0-or-later
 * @see	        <https://gitlab.com/jedoLabs/>
 */

namespace JedoLabs\ContaoExtensionHelperBundle\Dca;

use Contao\System;

class DcaUtil
{

    public static function createCheckBox($fieldname, $class, $bundle, $table, $onchange, $default)
    {
        return array
        (
            'label'     =>[
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.0'),
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.1'),
            ],
            'exclude'   => true,
            'default'   => ($onchange) ? '' : $default,
            'inputType' => 'checkbox',
            'eval'      => array('tl_class' => $class, 'submitOnChange' => $onchange),
            'sql'       => ($onchange) ? "char(1) NOT NULL default ''" : "char(1) NOT NULL default '1'"
        );
    }

    public static function createCheckBoxClearingAfter($fieldname, $bundle, $table, $onchange)
    {
        return array
        (
            'label'     =>[
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.0'),
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.1'),
            ],
            'exclude'   => true,
            'default'   => ($onchange) ? '' : '1',
            'inputType' => 'checkbox',
            'eval'      => array('tl_class' => 'clr m12', 'submitOnChange' => $onchange),
            'sql'       => ($onchange) ? "char(1) NOT NULL default ''" : "char(1) NOT NULL default '1'"
        );
    }

    public static function createCheckBoxOneLable($fieldname, $class, $bundle, $table, $onchange, $default)
    {
        return array
        (
            'label'     =>[
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname)
            ],
            'exclude'   => true,
            'default'   => ($onchange) ? '' : $default,
            'inputType' => 'checkbox',
            'eval'      => array('tl_class' => $class, 'submitOnChange' => $onchange),
            'sql'       => ($onchange) ? "char(1) NOT NULL default ''" : "char(1) NOT NULL default '1'"
        );
    }

    public static function createTextArea($fieldname, $bundle, $table, $rte)
    {
        return array
        (
            'label'     =>[
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.0'),
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.1'),
            ],
            'exclude'   => true,
            'inputType' => 'textarea',
            'eval'      => array('cols' => 30, 'rows' => 4, 'rte' => $rte),
            'sql'       => "text NULL"
        );
    }

    public static function createTextField($fieldname, $class, $bundle, $table, $default, $varScss)
    {
        return array
        (
            'label'     =>[
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.0'),
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.1'),
            ],
            'var_scss'  => $varScss,
            'default'       => $default,
            'exclude'   => true,
            'inputType' => 'text',
            'eval'      => array('tl_class' => $class, 'maxlength' => 64),
            'sql'       => "varchar(255) NOT NULL default ''"
        );
    }

    public static function createSelectBox($fieldname, $bundle, $table, $default, $options, $onchange, $blankOption)
    {

        $refs = "";

        foreach ($options as $ref)
        {
            $GLOBALS['TL_LANG'][$table][$ref] = System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.'. $ref);
        }

        return array
        (
            'label'         =>[
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.0'),
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.1'),
            ],
            'exclude'       => true,
            'default'       => $default,
            'options'       => $options,
            //'reference'     => [$refs],
            'reference'     => &$GLOBALS['TL_LANG'][$table],
            'inputType'     => 'select',
            'eval'          => array('includeBlankOption' => $blankOption, 'chosen' => true, 'tl_class' => 'w50', 'submitOnChange' => $onchange),
            'sql'           => "varchar(255) NOT NULL default ''"
        );
    }

    public static function createSelectBoxOptionCallback($fieldname, $bundle, $table, $default, $options, $onchange, $blankOption, $reference)
    {

        if($reference == true) {
            $refs = "";

            foreach ($options as $ref)
            {
                $GLOBALS['TL_LANG'][$table][$ref] = System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.'. $ref);
            }
        }

        return array
        (
            'label'         =>[
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.0'),
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.1'),
            ],
            'exclude'       => true,
            'sorting'       => true,
            'filter'        => true,
            'default'       => $default,
            //'options'       => $options,
            //'reference'     => [$refs],
            //'reference'     => &$GLOBALS['TL_LANG'][$table],
            'inputType'     => 'select',
            'options_callback'        => $options,
            'eval'          => array('includeBlankOption' => $blankOption, 'chosen' => true, 'tl_class' => 'w50', 'submitOnChange' => $onchange),
            'sql'           => "varchar(255) NOT NULL default ''"
        );
    }

    public static function createListField($fieldname, $class, $bundle, $table, $allowHtml)
    {
        return array
        (
            'label'     =>[
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.0'),
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.1'),
            ],
            'exclude'   => true,
            'inputType' => 'listWizard',
            'eval'      => array('allowHtml' => $allowHtml, 'tl_class' => $class),
            'xlabel'    => '',
            'sql'       => "blob NULL"
        );
    }

    public static function createInputUnitField ($fieldname, $bundle, $table, $default, $options, $onchange, $blankOption, $varScss, $preScss, $postScss)
    {
        if($varScss) {
            $GLOBALS['TL_CONFIG'][$table][$fieldname]['var_scss'] = $varScss;
        }
        if($preScss) {
            $GLOBALS['TL_CONFIG'][$table][$fieldname]['pre_scss'] = $preScss;
        }
        if($postScss) {
            $GLOBALS['TL_CONFIG'][$table][$fieldname]['post_scss'] = $postScss;
        }

        return array
        (
            'label'     =>[
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.0'),
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.1'),
            ],
            'var_scss'  => $varScss,
            'default'   => $default,
            'options'   => $options,
            'inputType' => 'inputUnit',
            'eval'      => array('includeBlankOption' => $blankOption, 'rgxp'=>'digit_normal_inherit', 'maxlength' => 32, 'tl_class' => 'w50', 'submitOnChange' => $onchange),
            'sql'       => "varchar(64) NOT NULL default ''"
        );
    }

    public static function createColorPickerField ($fieldname, $class, $bundle, $table, $default, $multiple, $size, $colorpicker, $ishexcolor, $decodeEntities, $varScss, $preScss, $postScss)
    {

        if($varScss) {
            $GLOBALS['TL_CONFIG'][$table][$fieldname]['var_scss'] = $varScss;
        }
        if($preScss) {
            $GLOBALS['TL_CONFIG'][$table][$fieldname]['pre_scss'] = $preScss;
        }
        if($postScss) {
            $GLOBALS['TL_CONFIG'][$table][$fieldname]['post_scss'] = $postScss;
        }


        return array
        (
            'label'     =>[
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.0'),
                System::getContainer()->get('translator')->trans('jedolabs.'.$bundle.'.'.$table.'.'. $fieldname .'.1'),
            ],
            'var_scss'  => $varScss,
            'pre_scss'  => $preScss,
            'post_scss' => $postScss,
            'default'   => $default,
            'exclude'   => true,
            'inputType' => 'text',
            'eval'      => array('tl_class' => $class, 'multiple'=>$multiple, 'maxlength'=>6, 'size'=>$size, 'colorpicker'=>$colorpicker, 'isHexColor'=>$ishexcolor, 'decodeEntities'=>true),
            'sql'       => "varchar(255) NOT NULL default ''"

        );
    }

}
