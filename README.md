# Contao 4 utils bundle

Contao is an Open Source PHP Content Management System for people who want a
professional website that is easy to maintain. Visit the [project website][1]
for more information.

This bundle offers various utility functionality for the contao 4 extension bundles by jedoLabs.

## Install

This bundle is only needed for the Contao extensions of jedoLabs, so a separate installation 
in your Contao CMS is not necessary.

Visit the [jedoLabs project website][2] for more information.


[1]: https://contao.org
[2]: https://jedo-labs.de
