<?php

/*
 * This file is part of ContaoExtensionHelperBundle.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * This project is provided in good faith and hope to be usable by anyone.
 *
 * @package     ContaoExtensionHelperBundle
 * @author      Jens Doberenz <WurzelGnOOm>
 * @copyright   jedoLabs 2019 <https://jedo-Labs.de>
 * @license     LGPL-3.0-or-later
 * @see	        <https://gitlab.com/jedoLabs/>
 */

namespace JedoLabs\ContaoExtensionHelperBundle\Tests;

use JedoLabs\ContaoExtensionHelperBundle\ContaoExtensionHelperBundle;
use PHPUnit\Framework\TestCase;

class ContaoExtensionHelperBundleTest extends TestCase
{
    public function testCanBeInstantiated()
    {
        $bundle = new ContaoExtensionHelperBundle();

        $this->assertInstanceOf(ContaoExtensionHelperBundle::class, $bundle);
    }
}
